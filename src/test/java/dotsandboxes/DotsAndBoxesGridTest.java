package dotsandboxes;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DotsAndBoxesGridTest {
    /*
     * Because Test classes are classes, they can have fields, and can have static
     * fields.
     * This field is a logger. Loggers are like a more advanced println, for writing
     * messages out to the console or a log file.
     */
    private static final Logger logger = LogManager.getLogger(DotsAndBoxesGridTest.class);

    /*
     * Tests are functions that have an @Test annotation before them.
     * The typical format of a test is that it contains some code that does
     * something, and then one
     * or more assertions to check that a condition holds.
     *
     * This is a dummy test just to show that the test suite itself runs
     */

    @Test
    public void boxCompleteDetectsCompletedBoxes() {
        DotsAndBoxesGrid case1 = new DotsAndBoxesGrid(2, 2, 2);
        case1.drawHorizontal(0, 0, 0);
        case1.drawHorizontal(0, 1, 0);
        case1.drawVertical(0, 0, 1);
        case1.drawVertical(1, 0, 1);
        assertTrue(case1.boxComplete(0, 0));
    }

    @Test
    public void boxCompleteDetectsIncompleteBoxes() {
        DotsAndBoxesGrid topHorizontal = new DotsAndBoxesGrid(2, 2, 2);
        DotsAndBoxesGrid botHorizontal = new DotsAndBoxesGrid(2, 2, 2);
        DotsAndBoxesGrid leftVertical = new DotsAndBoxesGrid(2, 2, 2);
        DotsAndBoxesGrid rightVertical = new DotsAndBoxesGrid(2, 2, 2);

        // top horizontal not drawn
        topHorizontal.drawHorizontal(0, 1, 0);
        topHorizontal.drawVertical(0, 0, 1);
        topHorizontal.drawVertical(1, 0, 1);

        // bot horizontal not drawn
        botHorizontal.drawHorizontal(0, 0, 0);
        botHorizontal.drawVertical(0, 0, 1);
        botHorizontal.drawVertical(1, 0, 1);

        // left vertical not drawn
        leftVertical.drawHorizontal(0, 0, 0);
        leftVertical.drawHorizontal(0, 1, 0);
        leftVertical.drawVertical(1, 0, 1);

        // right vertical not drawn
        rightVertical.drawHorizontal(0, 0, 0);
        rightVertical.drawHorizontal(0, 1, 0);
        rightVertical.drawVertical(0, 0, 1);

        assertFalse(topHorizontal.boxComplete(0, 0));
        assertFalse(botHorizontal.boxComplete(0, 0));
        assertFalse(leftVertical.boxComplete(0, 0));
        assertFalse(rightVertical.boxComplete(0, 0));
    }

    @Test
    public void drawMethodsDetectRedrawnLines() {
        DotsAndBoxesGrid case2 = new DotsAndBoxesGrid(2, 2, 2);
        case2.drawHorizontal(0, 0, 0);
        case2.drawVertical(0, 0, 1);
        assertThrows(RuntimeException.class, () -> case2.drawHorizontal(0, 0, 0));
        assertThrows(RuntimeException.class, () -> case2.drawVertical(0, 0, 0));
    }

}
